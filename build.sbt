name := "grammarFuzz"

version := "0.1"

scalaVersion := "2.12.6"

resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"
//resolvers += "Maven repo1" at "http://repo1.maven.org/"

//libraryDependencies += "com.typesafe.akka" % "akka-actor" % "2.0.2"

libraryDependencies ++= Seq(
  "org.scala-lang" % "scala-library" % "2.12.6",
  "log4j" % "log4j" % "1.2.17",
  "commons-io" % "commons-io" % "2.4",
  "org.antlr" % "antlr4" % "4.7",
  "com.github.scopt" %% "scopt" % "3.7.0",
  "com.github.mifmif" % "generex" % "1.0.2",
  "org.apache.commons" % "commons-lang3" % "3.7",
)

compileOrder := CompileOrder.Mixed

// for debugging sbt problems
logLevel := Level.Debug

//scalacOptions += "-deprecation"


assemblyMergeStrategy in assembly := {
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case x => MergeStrategy.first
}

//packMain := Map("hello" -> "driver.Main")

exportJars := true

//mainClass in (Compile, packageBin) := Some("driver.Main")

mainClass in assembly := Some("driver.Main")