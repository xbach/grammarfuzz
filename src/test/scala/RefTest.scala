import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

object RefTest {
  def main(args: Array[String]): Unit = {
    val test = new mutable.HashMap[String, ArrayBuffer[String]]()
    val x = new ArrayBuffer[String]()
    x.append("xv")
    test += "x" -> x
    println(test)
    x.clear()
    println(test)
  }
}
