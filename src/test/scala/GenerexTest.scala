object GenerexTest {
  def main(args: Array[String]): Unit = {
    import com.mifmif.common.regex.Generex
    val a = "[0-9]+"
    val b = "[0-3]([a-c]|[e-g]{1,2})"
    val c = "+"
    val generex = new Generex(c)//

    // Generate random String
    val randomStr = generex.random(1, 2)
    System.out.println(randomStr) // a random value from the previous String list


    // generate the second String in lexicographical order that match the given Regex.
    /*val secondString = generex.getMatchedString(2)
    System.out.println(secondString) // it print '0b'


    // Generate all String that matches the given Regex.
    val matchedStrs = generex.getAllMatchedStrings

    // Using Generex iterator
    val iterator = generex.iterator
    while ( {
      iterator.hasNext
    }) System.out.print(iterator.next + " ")*/
  }
}
