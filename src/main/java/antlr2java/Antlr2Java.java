package antlr2java;

import gov.nasa.jpf.vm.Verify;

public class Antlr2Java {
    static int depth = 0;
    static int MAX_DEPTH = 4;
    static int MAX_LENGTH = 6;

    public static void main(String args[]){
        //test1();
        String res = testGrammar2();
        if(!res.contains("ERROR")) {
            //Verify.terminateSearch();
            System.out.println("====\n" + res + "\n====\n");
        }
    }

    public static String testGrammar2(){
        depth += 1;
        int j = depth;
        if(depth < MAX_DEPTH) {
            if (Verify.randomBool()) {
                int length = 0;
                String x1 = testGrammar2();
                depth = j;
                length += x1.replace(" ", "").length();
                String x2 = " + ";
                length += 1;
                String x3 = testGrammar2();
                depth = j;
                length += x3.replace(" ", "").length();
                if (length < MAX_LENGTH) {
                    return x1 + x2 + x3;
                } else {
                    return "ERROR";
                }
                //else return "";
            } else if (Verify.randomBool()) {
                int length = 0;
                String x1 = testGrammar2();
                depth = j;
                length += x1.replace(" ", "").length();
                String x2 = " - ";
                length += 1;
                String x3 = testGrammar2();
                depth = j;
                length += x3.replace(" ", "").length();
                if (length < MAX_LENGTH) {
                    return x1 + x2 + x3;
                } else {
                    return "ERROR";
                }
            } else {
                return " 1 ";
            }
        }
        else return "ERROR";
    }

    public static String testGrammar(){
        depth += 1;
        int j = depth;
        if(depth < 4) {
            if (Verify.randomBool()) {
                String x1= testGrammar();
                depth = j;
                String x2= " + ";
                String x3 = testGrammar();
                depth = j;
               //if(!x1.contains("ERROR") && !x3.contains("ERROR"))
                return x1+x2+x3;
               //else return "";
            } else if (Verify.randomBool()) {
                String x1= testGrammar();
                depth = j;
                String x2= " - ";
                String x3 = testGrammar();
                depth = j;
                //if(!x1.contains("ERROR") && !x3.contains("ERROR"))
                    return x1+x2+x3;
                //else return "";
            } else {
                return " 1 ";
            }
        }
        else return "ERROR";
    }

    public static void test1(){
        if(Verify.randomBool()){
            System.out.println("TRUE branch");
            if(Verify.randomBool()){
                System.out.println("TRUE true branch");
            } else
                System.out.println("TRUE false branch");
            System.out.println("TRUE branch end");
        } else {
            System.out.println("FALSE branch");
            if(Verify.randomBool()){
                System.out.println("FALSE true branch");
            } else
                System.out.println("FALSE false branch");
            System.out.println("FALSE branch end");
        }
    }
}
