import java.util.Random

import config.MyConfig
import generator.{DataGenerator, FailedList, ParserGenerator, TestResult}
import mutators.{CandidateGrammar, GrammarMutator}
import org.apache.log4j.Logger
import parser.Parser

abstract class InterestingTestType(test: String, result: TestResult){
  def getTest():String = test
  def getResult(): TestResult = result
}
case class FailOracPassGram(test: String, result: TestResult) extends InterestingTestType(test, result)
case class PassOracFailGram(test: String, result: TestResult) extends InterestingTestType(test, result)

object Main {
  val logger = Logger.getLogger(this.getClass)

  def main(args: Array[String]): Unit = {
    val config = MyConfig.parse(args)
    val rand = new Random(config.randomSeed)
    val parserOrig = new Parser(config.grammarFile.getAbsolutePath)

    val parserGenOrig = new ParserGenerator(config.grammarFile, config.antlrDep)
    parserGenOrig.generateParserFromGrammar()
    parserGenOrig.compile()

    val mutator = new GrammarMutator(parserOrig.astInfor)

    val dataGen = new DataGenerator(rand, config.maxRepeat, parserOrig.astInfor, config.maxDepth)
    val s = dataGen.generate()
    // Debugging purpose only
    logger.debug(s)
    logger.debug(config.randomSeed)
    dataGen.stackRes.forEach(s => logger.debug(s.getText))
    // End debugging

    // Now generate additional test inputs using AFL
    // TODO
    val testFile = "/home/dxble/workspace/parsergen/src/main/resources/exp.txt"
    val testResult = parserGenOrig.invokeTest(testFile)
    logger.debug(testResult)
    // End generating test inputs by AFL. This returns interestingTests

    val interestingTests: Array[InterestingTestType] = Array(PassOracFailGram(testFile, testResult))
    val passOFailG = interestingTests.filter(_.isInstanceOf[PassOracFailGram])

    // Now do repair of the grammar G
    val cand = new CandidateGrammar
    val testResults = passOFailG.map(t => t.getResult())
    testResults.foreach(r => {
      r.asInstanceOf[FailedList].flist.foreach(f => {
        mutator.starNonTerm(cand, f.failedRule)
      })
    })

    logger.debug(cand.toGrammar(parserOrig.rewriter))

  }

  /*println("==============")
   astInfor.lexerRules.foreach{case (n, r) =>println(n + "->" + r.foldLeft(""){ (res, c) => res +" "+ rewriter.getText(c.getSourceInterval)})}

   astInfor.parserRules.foreach{case (n, r) => println(n + "->" + r.foldLeft(""){ (res, c) => res +" "+ rewriter.getText(c.getSourceInterval)})}
   println("===============")
   astInfor.ruleNonTerms.foreach(f => println(f._1+ " -> "+f._2.foldLeft("")((res, c) => res + rewriter.getText(c.getSourceInterval))))
   println("===============")
   astInfor.ruleTerms.foreach(f => println(f._1+ " -> "+f._2.foldLeft("")((res, c) => res + rewriter.getText(c.getSourceInterval))))

   val mutator = new GrammarMutator(astInfor)
   val cand = new CandidateGrammar
   mutator.nonTerminalReplacement(cand, null)
   mutator.terminalReplacement(cand, null)
   mutator.nonTerminalDeletion(cand, null)
   mutator.terminalDeletion(cand, null)
   mutator.nonTermDuplication(cand, null)
   mutator.termDuplication(cand, null)
   mutator.starProduction(cand, null)
   cand.chromosomes.foreach(c => println(c.toGrammarString(rewriter)))
   println(cand.toGrammar(rewriter))*/
}
