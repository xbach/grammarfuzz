package generator

import java.util
import java.util.Random
import java.util.regex.PatternSyntaxException

import antlrparser.ANTLRv4Parser.{EbnfSuffixContext, ElementContext}
import antlrparser.{ANTLRv4Parser, ANTLRv4ParserBaseVisitor}
import com.mifmif.common.regex.Generex
import org.antlr.v4.runtime.ParserRuleContext
import org.antlr.v4.runtime.tree.RuleNode
import parser.ASTInforCollector
import utils.Utils

import scala.collection.mutable

sealed trait Result
case class SomeResult(str: String) extends Result
case class EmptyResult() extends Result
case class FailureResult() extends Result

/**
  * Randomly walk the parse tree to generate a concrete inputs accepted by the tree
  * @param rand
  * @param repeat: maximum number of repetitions that should be generated
  */
class DataGenerator(rand: Random, repeat: Int, astInfor: ASTInforCollector, maxDepth: Int) extends ANTLRv4ParserBaseVisitor[Result] {

  val stackRes = new util.Stack[ParserRuleContext]()

  // Borrow from super class code. It is put here for debugging purpose only.
  override def visitChildren(node: RuleNode): Result = {
    var result = defaultResult
    val n = node.getChildCount
    var i = 0
    while (i < n) {
      if (!shouldVisitNextChild(node, result)) {
        //println("Not visit next child of: " + node.getText +" due to result = " + result)
        return result
      }

      val c = node.getChild(i)
      //println("-----Visiting child: "+c.getText)
      val childResult = c.accept(this)
      result = aggregateResult(result, childResult)
      i += 1
    }

    return result
  }

  def generate(ruleRef: String = astInfor.entryParserRuleName): Result = {
    val excludes = new mutable.HashSet[Int]()
    var res: Result = FailureResult()
    var chosenRule: ParserRuleContext = null
    val rules = astInfor.parserRules.getOrElse(ruleRef, null)
    if(maxDepth <= 0)
      return res

    while (excludes.size < rules.size && res.isInstanceOf[FailureResult]) {
      val index = Utils.nextIntInRangeButExclude(rand, 0, rules.size, excludes)
      chosenRule = rules(index)
      //println("Visiting rule: "+chosenRule.getText + "excl: "+ excludes + " of ruleRef: "+ruleRef + " maxDepth = "+maxDepth)
      stackRes.push(chosenRule)
      val generator = new DataGenerator(rand, repeat, astInfor, maxDepth - 1)
      res = chosenRule.accept(generator)
      if(res.isInstanceOf[FailureResult]) {
        stackRes.pop()
        excludes.add(index)
      } else
        generator.stackRes.forEach(s => stackRes.push(s))
    }

    if(!res.isInstanceOf[FailureResult]) {

      //println("Visited rule: " + chosenRule.getText + " res =" + res + " excl: " + excludes + " of ruleRef: " + ruleRef + " maxDepth = " + maxDepth)
    }
    res
  }

  private def isRegex(str: String): Boolean = {
    !str.contains("'")
    /*try {
      java.util.regex.Pattern.compile(str)
      true
    } catch {
      case e: PatternSyntaxException =>
        false
    }*/
  }

  override def visitAtom(ctx: ANTLRv4Parser.AtomContext): Result = {
    if(ctx.terminal() != null){
      if(ctx.terminal().TOKEN_REF() != null) {
        val refName = ctx.terminal().TOKEN_REF().getText
        //TODO: choose rules and expand rules for lexer. For now, we assume lexer already give exact string that we need.
        if(refName.compareTo("EOF") == 0) SomeResult("")
        else {
          val regex = astInfor.lexerRules.getOrElse(refName, null)(0).getText
          if (!isRegex(regex)){
            //println(regex)
            SomeResult(regex.replace("'", ""))
          } else {
            val generex = new Generex(regex, rand)
            //println("Regex: "+regex)
            val randomStr = generex.random(1, 2)
            SomeResult(randomStr)
          }
        }

      } else SomeResult(ctx.terminal().STRING_LITERAL().getText)
    } else if(ctx.ruleref() != null){
      generate(ctx.ruleref().RULE_REF().getText)
    } else { //TODO: notSet and DOT should be handled
      SomeResult("[TODO: notSet/DOT]")
    }
  }

  private def genTimesBySuffix(e: EbnfSuffixContext, times: Int): Int = {
    if (e != null) {
      if(e.STAR() != null)
        Utils.nextIntInRangeButExclude(rand, 0, times, new mutable.HashSet[Int]())
      else if(e.PLUS() != null) {
        Utils.nextIntInRangeButExclude(rand, 1, times, new mutable.HashSet[Int]())
      } else // (e.QUESTION() != null && e.QUESTION(0) != null)
      {
        if(times == repeat)
          Utils.nextIntInRangeButExclude(rand, 0,2, new mutable.HashSet[Int]())
        else Utils.nextIntInRangeButExclude(rand, 0, times, new mutable.HashSet[Int]())
      }
    } else 1
  }

  override def visitAlternative(ctx: ANTLRv4Parser.AlternativeContext): Result = {
    import scala.collection.JavaConverters._
    val collectedElements = new util.LinkedList[ElementContext]()
    val ruleExecTimes = new mutable.HashMap[ElementContext,Int]()

    def helper(e: ElementContext): Boolean = {
      val curTimes = ruleExecTimes.getOrElse(e, repeat)
      val genTimes = genTimesBySuffix(e.ebnfSuffix(), curTimes)
      ruleExecTimes.update(e, genTimes)
      if(curTimes == genTimes)
        return false

      var index = 0
      while (index < genTimes){
        collectedElements.add(e)
        index += 1
      }
      if(index > 0)
        true
      else false
    }

    ctx.element().asScala.foreach(helper(_))
    var allRes: Result = EmptyResult()
    while(!collectedElements.isEmpty && !allRes.isInstanceOf[FailureResult]){
      val e = collectedElements.pop()
      var resE: Result = FailureResult()
      var res = false
      while(!res){
        resE = e.accept(this)
        if(resE.isInstanceOf[FailureResult]){
          val times = ruleExecTimes.getOrElse(e, 0)
          var p = 1
          while (p < times){
            collectedElements.pop()
            p += 1
          }
          if(p == 1 || !helper(e))
            res = true
        } else {
          res = true
        }
      }
      allRes = aggregateResult(allRes, resE)
    }
    allRes
  }

  override def visitEbnf(ctx: ANTLRv4Parser.EbnfContext): Result = {
    throw new RuntimeException("Need to handle ebnf ....")
    /*
    val genTimes = if(ctx.blockSuffix() != null) genTimesBySuffix(ctx.blockSuffix().ebnfSuffix(), repeat) else 1
    var index = 0
    var res: Result = EmptyResult()
    while (index < genTimes){
      res = aggregateResult(res, this.visitBlock(ctx.block()))
      index += 1
    }
    res */
  }

  /*override def visitBlock(ctx: ANTLRv4Parser.BlockContext): String = {
    this.visitAltList(ctx.altList())
  }*/

  override def visitLabeledElement(ctx: ANTLRv4Parser.LabeledElementContext): Result = {
    SomeResult("[TODO visitLabeledELement]")
  }

  override def shouldVisitNextChild(node: RuleNode, currentResult: Result): Boolean = {
    currentResult match {
      case _: FailureResult => {
        false
      }
      case _ => {
        true
      }
    }
  }

  override def defaultResult(): Result = EmptyResult()

  override def aggregateResult(aggregate: Result, nextResult: Result): Result = {
    val res = aggregate match {
      case FailureResult() =>  aggregate
      case EmptyResult() =>  nextResult
      case SomeResult(a) => {
        nextResult match {
          case FailureResult() =>  nextResult
          case EmptyResult() =>  aggregate
          case SomeResult(n) =>  SomeResult(a + n)
        }
      }
    }
    //println("Aggregated res = "+res)
    res
  }
}
