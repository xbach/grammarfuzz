package generator

import java.io.{File, IOException}
import java.lang.reflect.Method
import java.util

import compiler.JavaJDKCompiler
import org.antlr.v4.Tool
import org.antlr.v4.tool.ErrorType
import org.apache.log4j.Logger
import utils.Utils

sealed trait TestResult
case class Success() extends TestResult
case class Failed(line: Int, charInLine: Int, failedRule: String) extends TestResult
case class FailedList(flist: Array[Failed]) extends TestResult{
  override def toString: String = {
    flist.foldLeft(""){(res, i) => res+i+";"}
  }
}
case class FailedByException() extends TestResult

abstract class ParserExecution(grammarFile: File){

  private def antlrTool(args: Array[String]): Unit = {
    val antlr = new Tool(args)
    if (args.length == 0) {
      antlr.help()
      antlr.exit(0)
    }

    try
      antlr.processGrammarsOnCommandLine()
    finally if (antlr.log) try {
      val logname = antlr.logMgr.save
      System.out.println("wrote " + logname)
    } catch {
      case ioe: IOException =>
        antlr.errMgr.toolError(ErrorType.INTERNAL_ERROR, ioe)
    }
  }

  def generateParserFromGrammar() = {
    val dir = grammarFile.getParentFile.getParentFile.getAbsolutePath
    val args: Array[String] = Array[String]("-o", dir+"/java/pgen", "-package", "pgen", "-lib", dir+"/resources/", grammarFile.getAbsolutePath)
    antlrTool(args)
  }

  def compile(): Unit
}
class ParserGenerator(grammarFile: File, dep: String) extends ParserExecution(grammarFile) {
  val logger = Logger.getLogger(this.getClass)
  var main: Method = null

  /**
    * We do in-memory compilation for possibly faster speed of compiling and running parser on a given input test
    */
  override def compile(): Unit = {
    val baseDir = grammarFile.getParentFile.getParentFile.getParentFile.getParentFile
    val classDir = baseDir + "/target/classes"
    val sourceDir = baseDir + "/src/main/java"
    val sourceFilesArray: Array[String] = Utils.getSourceFiles(Array[String](sourceDir))
    val sourceFileContents: util.HashMap[String, String] = Utils.buildSourceDocumentMap(sourceFilesArray, sourceDir)
    val toCompile: util.HashMap[String, String] = new java.util.HashMap[String, String]()
    val ent = sourceFileContents.entrySet().iterator()
    while (ent.hasNext) {
      val next = ent.next()
      toCompile.put(next.getKey, next.getValue)
    }
    val classPaths: util.ArrayList[String] = new util.ArrayList[String]()
    dep.split(":").foreach(d => classPaths.add(d))
    val compiler: JavaJDKCompiler = new JavaJDKCompiler(classDir, classPaths, toCompile, null, null)
    logger.debug(compiler.compile())
    val target = compiler.getMcl.loadClass("runner.ParserRunner")
    //main = target.getMethod("main", Array[String]().getClass)
    main = target.getMethod("runner", new String().getClass)
  }

  def invokeTest(test: String): TestResult = {
    //main.invoke(null, Array[String](test))
    val res = main.invoke(null, test).asInstanceOf[String]
    if(res == null)
      FailedByException()
    else if(res.contains("TestSuccess"))
      Success()
    else {
      val errIndex = res.split("\n").filter(_.contains("TestFail:")).map(_.split(":").drop(1))
      val result = errIndex.map(i => {
        val s = i(0).split(";")
        Failed(s(0).toInt, s(1).toInt, i(1))
      })
      FailedList(result)
    }
  }
}
