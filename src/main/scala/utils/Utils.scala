package utils

import java.io.File

import scala.collection.mutable
import scala.util.Random
import java.nio.file.Paths
import java.util.{Collection, HashMap, LinkedList}

import org.apache.commons.io.FileUtils
import org.apache.commons.io.filefilter.{SuffixFileFilter, TrueFileFilter}

object Utils {

  import java.nio.file.Files
  import java.nio.file.Paths

  /**
    * Read function for JVM >= 1.7
    *
    * @param file
    * @return
    * @throws Exception
    */
  @throws[Exception]
  def readFromFile(file: File): Array[Byte] = {
    val bytes = Files.readAllBytes(Paths.get(file.getPath))
    bytes
  }

  def relativePath(base: String, pathFull: String): String = {
    val pathAbsolute = Paths.get(pathFull)
    val pathBase = Paths.get(base)
    val pathRelative = pathBase.relativize(pathAbsolute)
    pathRelative.toString
  }

  /**
    * @param start    start of range (inclusive)
    * @param end      end of range (exclusive)
    * @param excludes numbers to exclude (= numbers you do not want)
    * @return the random number within start-end but not one of excludes
    */
  def nextIntInRangeButExclude(random: Random, start: Int, end: Int, excludes: mutable.HashSet[Int]): Int = {
    try {
      val rangeLength = end - start - excludes.size
      //println(rangeLength + " " + excludes + " " + start + " " + end)
      var randomInt = random.nextInt(rangeLength) + start
      val ss = collection.mutable.SortedSet(excludes.toList: _*)
      ss.foreach { e => {
        if (e > randomInt) return randomInt
        randomInt += 1
      }
      }
      randomInt
    }catch {
      case e: Exception => {
        println("debug")
        0
      }
    }
  }

  // Utils for compiler

  /**
    * Generates a list of java source files given a directory, or returns the
    * file specified in an array.
    * @param sourcePaths The path to the file/directory.
    * @return An array of paths to Java source files.
    * @throws Exception
    */
  @throws(classOf[Exception])
  def getSourceFiles(sourcePaths: Array[String], ext: String = ".java"): Array[String] = {
    val sourceFiles: Collection[File] = new LinkedList[File]
    var sourceFilesArray: Array[String] = null
    for (sourcePath <- sourcePaths) {
      val sourceFile: File = new File(sourcePath)
      if (sourceFile.isDirectory) {
        sourceFiles.addAll(FileUtils.listFiles(sourceFile, new SuffixFileFilter(ext), TrueFileFilter.INSTANCE))
      }
      else {
        sourceFiles.add(sourceFile)
      }
      sourceFilesArray = new Array[String](sourceFiles.size)
      var i: Int = 0
      import scala.collection.JavaConversions._
      for (file <- sourceFiles) {
        sourceFilesArray(i) = file.getCanonicalPath
        i += 1
      }
    }
    return sourceFilesArray
  }

  /**
    * Builds a HashMap with Java file paths as keys and Java file text contents as values.
    * @param sourceFilesArray
    * @return A HashMap containing the text of the source Java files.
    */
  @throws(classOf[Exception])
  def buildSourceDocumentMap(sourceFilesArray: Array[String], sourcePath: String): HashMap[String, String] = {
    val map: HashMap[String, String] = new HashMap[String, String]
    for (sourceFile <- sourceFilesArray) {
      val backingFile: File = new File(sourceFile)
      val encoded: Array[Byte] = Utils.readFromFile(backingFile)
      val contents: String = new String(encoded)
      val temp= Utils.relativePath(sourcePath, sourceFile).split("\\.")(0)
      map.put(temp, contents)
    }
    return map
  }
}
