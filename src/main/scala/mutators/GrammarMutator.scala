package mutators

import antlrparser.ANTLRv4Parser.{AlternativeContext, RulerefContext, TerminalContext}
import antlrparser.{ANTLRv4Parser, ANTLRv4ParserBaseListener}
import config.MyConfig
import org.antlr.v4.runtime.{ParserRuleContext, TokenStreamRewriter}
import parser.ASTInforCollector

import scala.collection.mutable.ArrayBuffer

case class CandidateGrammar(){
  lazy val chromosomes = new ArrayBuffer[MutationOps]()

  def toGrammar(rewriter: TokenStreamRewriter): String = {
    val pass = MyConfig.random.nextLong().toString
    chromosomes.foreach(c => c.applyMutation(rewriter, pass))
    rewriter.getText(pass)
  }
}

class GrammarMutator(astInfor: ASTInforCollector) {
  lazy val parserNonTerms = astInfor.parserRules.keys.toArray

  // By doing this, we assume that all terminals are defined via lexer rules.
  // Otherwise if a terminal is only declared directly in the parser rule, we will miss the terminal
  lazy val parserTerms = astInfor.lexerRules.keys.toArray

  private def chooseRandomProductionRule(ruleName: String = null): ParserRuleContext = {
    if(ruleName == null) {
      val i = MyConfig.random.nextInt(parserNonTerms.length)
      val rules = astInfor.parserRules.getOrElse(parserNonTerms(i), null)
      rules(MyConfig.random.nextInt(rules.length))
    } else {
      val rules = astInfor.parserRules.getOrElse(ruleName, null)
      rules(MyConfig.random.nextInt(rules.length))
    }
  }

  private def chooseRandomNonTermFromGrammar(rule: ParserRuleContext): RulerefContext = {
    var repeat = 0
    while (true && repeat <= 5) {
      repeat += 1
      val dest = if(rule == null) chooseRandomProductionRule() else rule
      val nonTermsOfDest = astInfor.ruleNonTerms.getOrElse(dest.getText, null).toArray
      if(!nonTermsOfDest.isEmpty) {
        val destNonTerm = nonTermsOfDest(MyConfig.random.nextInt(nonTermsOfDest.size))
        return destNonTerm
      }
    }
    return null
  }

  private def chooseRandomTermFromGrammar(rule: ParserRuleContext): TerminalContext = {
    var repeat = 0
    while (true && repeat <= 5) {
      repeat += 1
      val dest = if(rule == null) chooseRandomProductionRule() else rule
      val termsOfDest = astInfor.ruleTerms.getOrElse(dest.getText, null).toArray
      if(!termsOfDest.isEmpty) {
        val destTerm = termsOfDest(MyConfig.random.nextInt(termsOfDest.size))
        return destTerm
      }
    }
    return null
  }

  /**
    * Replace a non-terminal node with another non-terminal node
    * @param cand
    * @param beRep
    */
  def nonTerminalReplacement(cand: CandidateGrammar, beRep: ParserRuleContext = null): Unit = {
    val destNonTerm = chooseRandomNonTermFromGrammar(beRep)
    val sourceNonTerm = parserNonTerms(MyConfig.random.nextInt(parserNonTerms.length))
    cand.chromosomes.append(Replace(destNonTerm, sourceNonTerm, destNonTerm.getStart.getLine, destNonTerm.getStart.getCharPositionInLine))
  }

  def terminalReplacement(candidateGrammar: CandidateGrammar, beRep: ParserRuleContext = null): Unit = {
    val desTerm = chooseRandomTermFromGrammar(beRep)
    val sourceTerm = parserTerms(MyConfig.random.nextInt(parserNonTerms.length))
    candidateGrammar.chromosomes.append(Replace(desTerm, sourceTerm, desTerm.getStart.getLine, desTerm.getStart.getCharPositionInLine))
  }

  def nonTerminalDeletion(candidateGrammar: CandidateGrammar, beDel: ParserRuleContext = null): Unit = {
    val destNonTerm = chooseRandomNonTermFromGrammar(beDel)
    candidateGrammar.chromosomes.append(Delete(destNonTerm, destNonTerm.getStart.getLine, destNonTerm.getStart.getCharPositionInLine))
  }

  def terminalDeletion(candidateGrammar: CandidateGrammar, beDel: ParserRuleContext = null): Unit = {
    val destTerm = chooseRandomTermFromGrammar(beDel)
    candidateGrammar.chromosomes.append(Delete(destTerm, destTerm.getStart.getLine, destTerm.getStart.getCharPositionInLine))
  }

  def nonTermDuplication(candidateGrammar: CandidateGrammar, beDup: ParserRuleContext): Unit = {
    val desNonTerm = chooseRandomNonTermFromGrammar(beDup)
    candidateGrammar.chromosomes.append(InsertAfter(desNonTerm, desNonTerm, desNonTerm.getStart.getLine, desNonTerm.getStart.getCharPositionInLine, desNonTerm.getStop))
  }

  def termDuplication(candidateGrammar: CandidateGrammar, beDup: ParserRuleContext): Unit = {
    val desTerm = chooseRandomTermFromGrammar(beDup)
    candidateGrammar.chromosomes.append(InsertAfter(desTerm, desTerm, desTerm.getStart.getLine, desTerm.getStart.getCharPositionInLine, desTerm.getStop))
  }

  def starNonTerm(candidateGrammar: CandidateGrammar, ruleName: String): Unit = {
    val prod = chooseRandomProductionRule(ruleName)
    val nonTerms = new ANTLRv4ParserBaseListener {
      val nonTerminalNodes = new ArrayBuffer[RulerefContext]()

      override def enterRuleref(ctx: RulerefContext): Unit = {
        nonTerminalNodes.append(ctx)
      }
    }
    import org.antlr.v4.runtime.tree.ParseTreeWalker
    // Walk it and attach our listener
    val walker = new ParseTreeWalker
    walker.walk(nonTerms, prod)
    val randNode = nonTerms.nonTerminalNodes(MyConfig.random.nextInt(nonTerms.nonTerminalNodes.size))
    candidateGrammar.chromosomes.append(InsertStar(randNode, "*", randNode.getStart.getLine, randNode.getStart.getCharPositionInLine, randNode.getStart, randNode.getStop))
  }

  def starProduction(candidateGrammar: CandidateGrammar): Unit = {
    val prod = chooseRandomProductionRule()
    val alternativeCollector = new ANTLRv4ParserBaseListener {
      var alt: AlternativeContext = null
      override def enterAlternative(ctx: ANTLRv4Parser.AlternativeContext): Unit = {
        if(alt == null) // collect the first alternative only: see rule: labeledAlt: alternative (POUND identifier)?
          alt = ctx
      }
    }
    import org.antlr.v4.runtime.tree.ParseTreeWalker
    // Walk it and attach our listener
    val walker = new ParseTreeWalker
    walker.walk(alternativeCollector, prod)
    candidateGrammar.chromosomes.append(InsertStar(alternativeCollector.alt, "*", alternativeCollector.alt.getStart.getLine, alternativeCollector.alt.getStart.getCharPositionInLine, alternativeCollector.alt.getStart, alternativeCollector.alt.getStop))
  }

}
