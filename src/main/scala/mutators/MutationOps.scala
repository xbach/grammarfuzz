package mutators

import org.antlr.v4.runtime.{Token, TokenStreamRewriter}
import org.antlr.v4.runtime.tree.ParseTree

sealed trait MutationOps{
  def toGrammarString(rewriter: TokenStreamRewriter): String
  def applyMutation(rewriter: TokenStreamRewriter, pass: String): Unit
}
case class Replace(dest: ParseTree, source: String, start: Int, end: Int) extends MutationOps{
 override def toGrammarString(rewriter: TokenStreamRewriter): String = "Replace(" +rewriter.getText(dest.getSourceInterval)+ "["+ start+".."+end +"]"+"," + source+")"

  override def applyMutation(rewriter: TokenStreamRewriter, pass: String): Unit = rewriter.replace(pass, dest.getSourceInterval.a, dest.getSourceInterval.b, source)
}
case class Delete(toDel: ParseTree, start: Int, end: Int) extends MutationOps{
  override def toGrammarString(rewriter: TokenStreamRewriter): String = "Delete(" +rewriter.getText(toDel.getSourceInterval)+ "["+ start+".."+end +"])"

  override def applyMutation(rewriter: TokenStreamRewriter, pass: String): Unit = rewriter.delete(pass, toDel.getSourceInterval.a, toDel.getSourceInterval.b)
}
case class InsertAfter(dest: ParseTree, source: ParseTree, start: Int, end: Int, stop: Token) extends MutationOps {
  override def toGrammarString(rewriter: TokenStreamRewriter): String =  "InsertAfter(" +rewriter.getText(dest.getSourceInterval)+ "["+ start+".."+end +"]"+"," + rewriter.getText(source.getSourceInterval)+")"

  override def applyMutation(rewriter: TokenStreamRewriter, pass: String): Unit = rewriter.insertAfter(pass, stop, " "+source.getText)
}
case class InsertStar(dest: ParseTree, source: String, start: Int, end: Int, startToken: Token, stopToken: Token) extends MutationOps {
  override def toGrammarString(rewriter: TokenStreamRewriter): String =  "InsertStar(" +rewriter.getText(dest.getSourceInterval)+ "["+ start+".."+end +"]"+"," + source+")"

  override def applyMutation(rewriter: TokenStreamRewriter, pass: String): Unit = {
    rewriter.insertBefore(pass, startToken, "(")
    rewriter.insertAfter(pass, stopToken, ")"+source)
  }
}