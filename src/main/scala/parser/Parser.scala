package parser

import java.io.File

import antlrparser.ANTLRv4Parser.GrammarSpecContext
import antlrparser.{ANTLRv4Lexer, ANTLRv4Parser}
import org.antlr.v4.runtime.{BailErrorStrategy, CharStreams, CommonTokenStream, TokenStreamRewriter}
import org.apache.commons.io.FileUtils
import org.apache.log4j.Logger


class Parser(grammarFile: String) {

  val logger = Logger.getLogger(this.getClass)

  lazy val parser: ANTLRv4Parser = parse(grammarFile)
  lazy val rewriter: TokenStreamRewriter = new TokenStreamRewriter(parser.getTokenStream)
  lazy val astInfor: ASTInforCollector =  {
    val tree = parser.grammarSpec()
    // begin parsing at init rule
    val listener = new ASTInforCollector()
    import org.antlr.v4.runtime.tree.ParseTreeWalker
    // Walk it and attach our listener
    val walker = new ParseTreeWalker
    walker.walk(listener, tree)
    listener
  }

  private def parse(file: String): ANTLRv4Parser = {
    // create a CharStream that reads from standard input
    val inputString = FileUtils.readFileToString(new File(file))
    val input = CharStreams.fromString(inputString)
    //ANTLRInputStream input = new ANTLRInputStream("(set-logic  LIA)\n(check-synth)");
    // create a lexer that feeds off of input CharStream
    val lexer = new ANTLRv4Lexer(input)
    // create a buffer of tokens pulled from the lexer
    val tokens = new CommonTokenStream(lexer)
    // create a parser that feeds off the tokens buffer
    val parser = new ANTLRv4Parser(tokens)
    val errHandler = new BailErrorStrategy()
    parser.setErrorHandler(errHandler)

    //logger.debug(tree.toStringTree(parser)) // print LISP-style tree
    parser
  }


  def main(args: Array[String]): Unit = {
    /*val (listener,rewriter, tree) = parse("/home/dxble/workspace/grammarFuzz/src/main/resources/testgrammar.g4")
    println("==============")
    listener.lexerRules.foreach{case (n, r) =>println(n + "->" + r.foldLeft(""){ (res, c) => res +" "+ rewriter.getText(c.getSourceInterval)})}

    listener.parserRules.foreach{case (n, r) => println(n + "->" + r.foldLeft(""){ (res, c) => res +" "+ rewriter.getText(c.getSourceInterval)})}
    */
    //generateParserFromGrammar("")
  }
}