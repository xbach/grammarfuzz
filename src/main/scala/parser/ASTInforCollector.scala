package parser

import antlrparser.ANTLRv4Parser.{RulerefContext, TerminalContext}
import antlrparser.{ANTLRv4Parser, ANTLRv4ParserBaseListener}
import org.antlr.v4.runtime.ParserRuleContext

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

class ASTInforCollector extends ANTLRv4ParserBaseListener {
  lazy val parserRules = new mutable.LinkedHashMap[String, ArrayBuffer[ParserRuleContext]]()
  lazy val lexerRules = new mutable.LinkedHashMap[String, ArrayBuffer[ParserRuleContext]]()

  lazy val ruleNonTerms = new mutable.HashMap[String, mutable.HashSet[RulerefContext]]()
  lazy val ruleTerms = new mutable.HashMap[String, mutable.HashSet[TerminalContext]]()

  var entryParserRuleName: String = null

  // Temporary variables
  private lazy val nonTerminalNodes = new mutable.HashSet[RulerefContext]()
  private lazy val terminalNodes = new mutable.HashSet[TerminalContext]()

  private lazy val currentProductionRules = new ArrayBuffer[ParserRuleContext]()

  override def enterRuleSpec(ctx: ANTLRv4Parser.RuleSpecContext): Unit = {
    currentProductionRules.clear()
  }

  // Lexer rules
  override def exitLexerRuleSpec(ctx: ANTLRv4Parser.LexerRuleSpecContext): Unit = {
    val cpElems = new ArrayBuffer[ParserRuleContext]()
    currentProductionRules.foreach(cpElems.append(_))
    lexerRules += ctx.TOKEN_REF().getText -> cpElems
    currentProductionRules.clear()
  }

  override def enterLexerElements(ctx: ANTLRv4Parser.LexerElementsContext): Unit = {
    currentProductionRules.append(ctx)
  }

  // Parser rules
  override def enterParserRuleSpec(ctx: ANTLRv4Parser.ParserRuleSpecContext): Unit = {
    if(entryParserRuleName == null)
      entryParserRuleName = ctx.RULE_REF().getText
  }

  override def exitParserRuleSpec(ctx: ANTLRv4Parser.ParserRuleSpecContext): Unit = {
    val cpElems = new ArrayBuffer[ParserRuleContext]()
    currentProductionRules.foreach(cpElems.append(_))
    parserRules += ctx.RULE_REF().getText -> cpElems
    currentProductionRules.clear()
  }

  override def enterLabeledAlt(ctx: ANTLRv4Parser.LabeledAltContext): Unit = {
    currentProductionRules.append(ctx)

    nonTerminalNodes.clear()
    terminalNodes.clear()
  }

  // Collect terminal and non-terminals appearing in a production rule
  override def exitLabeledAlt(ctx: ANTLRv4Parser.LabeledAltContext): Unit = {
    val cp = new mutable.HashSet[RulerefContext]()
    nonTerminalNodes.foreach(cp.add(_))
    ruleNonTerms += ctx.getText -> cp

    val cpx = new mutable.HashSet[TerminalContext]()
    terminalNodes.foreach(cpx.add(_))
    ruleTerms += ctx.getText -> cpx

    nonTerminalNodes.clear()
    terminalNodes.clear()
  }

  override def enterRuleref(ctx: RulerefContext): Unit = {
    nonTerminalNodes.add(ctx)
  }

  override def enterTerminal(ctx: TerminalContext): Unit = {
    terminalNodes.add(ctx)
  }
}
