package config

import java.io.File

import scala.util.Random

case class MyConfig (grammarFile: File = null, antlrDep: String = "", maxDepth: Int = 10, maxRepeat: Int = 10, randomSeed: Int = 0){}

object MyConfig {
  val random = new Random()

  def parse(args: Array[String]): MyConfig = {
    val parser = new scopt.OptionParser[MyConfig]("scopt") {
      head("scopt", "3.x")

      opt[File]('g', "grammar").required().valueName("<file>").
        action( (x, c) => c.copy(grammarFile = x) ).
        text("Grammar file to be fuzzed")

      opt[Int]('d', "maxdepth").required().valueName("<int>").
        action( (x, c) => c.copy(maxDepth = x) ).
        text("Max depth when generating concrete input from grammar")

      opt[Int]('r', "maxrepeat").required().valueName("<int>").
        action( (x, c) => c.copy(maxDepth = x) ).
        text("Max repeat for rules such as * or + when generating concrete input from grammar")

      opt[Int]('s', "randomSeed").valueName("<int>").
        action( (x, c) => c.copy(maxDepth = x) ).
        text("Random seed number")

      opt[String]('a', "antlrDep").valueName("<string:string>").
        action( (x, c) => c.copy(antlrDep = x) ).
        text("Antlr Dependencies")
    }

    // parser.parse returns Option[C]
    parser.parse(args, MyConfig()) match {
      case Some(config) => config
      // do stuff

      case None => null
      // arguments are bad, error message will have been displayed
    }
  }
}
