package jpfrunner

package backend

import java.util

import gov.nasa.jpf.{Config, JPF, JPFConfigException, JPFException}
import org.apache.log4j.Logger

/**
  * Created by dxble on 9/2/16.
  */
object JpfRunner{
  def main(args: Array[String]): Unit = {
    val runner = new JpfRunner("/home/dxble/workspace/grammarFuzz/target/scala-2.12/classes", "/home/dxble/workspace/grammarFuzz/src/main/java/antlr2java", 20, "", "antlr2java.Antlr2Java")
    runner.run()
  }
}
class JpfRunner(deps: String, sourcePath: String, depth: Int, targetArgs: String, targetPkgClassName: String) {
  val logger = Logger.getLogger(this.getClass)
  var commonJpf: JPF = null

  def commonSPFConfig(): Unit = {
    if(commonJpf != null)
      return

    // this initializes the JPF configuration from default.properties, site.properties
    // configured extensions (jpf.properties), current directory (jpf.properies) and
    // command line args ("+<key>=<value>" options and *.jpf)
    val conf = JPF.createConfig(Array[String]())

    logger.debug("JPF classpath: "+deps)
    //conf.setProperty("@using","jpf-nhandler")
    //conf.setProperty("@using","jpf-symbc")
    conf.setProperty("classpath", deps)
    conf.setProperty("native_classpath", deps)

    //conf.setProperty("vm.reuse_tid","true")
    //conf.setProperty("nhandler.resetVMState","false")
    //conf.setProperty("log.level","finest") // @See http://babelfish.arc.nasa.gov/trac/jpf/wiki/user/output
    //Delegates are separated by ,
    //if(Options.nhandlerDelegates != null)
    //  conf.setProperty("nhandler.spec.delegate", Options.nhandlerDelegates)

    //conf.setProperty("nhandler.spec.skip", "java.util.regex.Matcher.end")
    //conf.setProperty("nhandler.delegateUnhandledNative", Options.nhandlerDelegateUnhandledNative.toString)
    //conf.setProperty("nhandler.genSource","true")
    //conf.setProperty("nhandler.clean","false")

    conf.setProperty("vm.classpath", deps)
    conf.setProperty("sourcepath", sourcePath)
    conf.setProperty("search.depth_limit", depth.toString)
    //conf.setProperty("search.multiple_errors", Options.jpfMultipleErrors.toString())
    conf.setProperty("target.args", targetArgs)

    // This is somewhat important. If the value lies below or over this thresholds, silent errors may happen...
    //conf.setProperty("symbolic.min_double",Options.jpfMinDouble.toString)
    //conf.setProperty("symbolic.max_double", Options.jpfMaxDouble.toString)
    //conf.setProperty("symbolic.min_int",Options.jpfMinInt.toString)
    //conf.setProperty("symbolic.max_int", Options.jpfMaxInt.toString)
    //conf.setProperty("search.multiple_errors", Options.jpfSearchMultipleErrors.toString)

    //conf.setProperty("symbolic.string_dp", "automata")
    // Set if we should print output of the SUT
    //conf.setProperty("vm.tree_output", Options.debug.toString)
    //conf.setProperty("symbolic.dp",Options.jpfSymbcSolver)
    conf.setProperty("symbolic.string_dp","automata")

    conf.setProperty("target", targetPkgClassName)
    val jpf = new JPF(conf)

    // ... explicitly create listeners (could be reused over multiple JPF runs)
    //val myListener: Symbolic = new MySymbolicListener(conf, jpf, symbcLocs)
    // ... set your listeners
    //jpf.addListener(myListener)
    commonJpf = jpf
  }

  def run(): Unit ={
    try {

      commonSPFConfig()

      //System.setProperty(S3Properties.RUNNING_TEST_NUMBER, test.getId().toString)
      //System.setProperty(S3Properties.DEBUG, Options.debug.toString)
      // Bachle: This tells jpf to get properties from the host VM
      // We have some properties that are passed through our RunTime.java during symbolic execution
      // Thus, setting this option is crucial. @See package backend RunTime.java
      //commonJpf.getConfig.setProperty("vm.sysprop.source", "host")

      commonJpf.run()
      //solvedPC = new ParseSolvedPC(false)

      if (commonJpf.foundErrors()){
        // ... process property violations discovered by JPF
      }
    } catch{
      case e: JPFConfigException =>{
        logger.debug("JPF config exception: "+e.getMessage)
        //solvedPC = new ParseSolvedPC(true)
      }
      // ... handle configuration exception
      // ...  can happen before running JPF and indicates inconsistent configuration data

      case e: JPFException => {
        //logger.info("JPF exception: "+e.getMessage)
        e.printStackTrace()
        //solvedPC = new ParseSolvedPC(true)
      }

      // ... handle exception while executing JPF, can be further differentiated into
      // ...  JPFListenerException - occurred from within configured listener
      // ...  JPFNativePeerException - occurred from within MJI method/native peer
      // ...  all others indicate JPF internal errors
      case e: RuntimeException => {
        // Note that: JPF may throw run time exception, for example, not handle symbolic index for arrays.
        logger.info("Runtime Exception while running JPF: "+e.getMessage)
        logger.info("Original exception: "+e)
        e.printStackTrace()
        println(e.getLocalizedMessage)
      }

    }finally {

    }
  }
}
