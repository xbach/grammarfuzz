grammar Calculator;
INT    : [0-9]+;
POW    : '^';
ROOT   : 'r';
NL     : '\n';
WS     : [ \t\r]+ -> skip;

PLUS  : '+';
EQUAL : '=';
MINUS : '-';
MULT  : '*';
DIV   : '/';
LPAR  : '(';
RPAR  : ')';

input
    : plusOrMinus NL? EOF # Calculate
    ;

plusOrMinus
    : plusOrMinus PLUS multOrDiv  # Plus
    | plusOrMinus MINUS multOrDiv # Minus
    | multOrDiv                   # ToMultOrDiv
    ;

multOrDiv
    : multOrDiv MULT pow # Multiplication
    | multOrDiv DIV pow  # Division
    | pow                # ToPow
    | root               # ToRoot
    ;

pow
    : unaryMinus (POW pow)? # Power
    ;

root
    : unaryMinus (ROOT root)? # Rt
    ;

unaryMinus
    : MINUS unaryMinus # ChangeSign
    | atom             # ToAtom
    ;

atom
    : INT                  # Int
    | LPAR plusOrMinus RPAR # Braces
    ;

     | exp MULT exp  # Mult
        | exp DIV exp # Div
        | exp POW exp # Pow
        | exp ROOT exp # Root