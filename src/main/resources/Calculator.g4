grammar Calculator;
INT    : [0-9]+/*^(0|[1-9][0-9]*)$*/;
POW    : '^';
ROOT   : 'r';
NL     : '\n';
WS     : [ \t\r]+ -> skip;

PLUS  : '+';
MINUS : '-';
MULT  : '*';
DIV   : '/';
LPAR  : '(';
RPAR  : ')';

input
    : exp NL? EOF # Calculate
    ;

exp
    : exp PLUS exp  # Plus
    | exp MINUS exp # Minus
    | LPAR exp RPAR # Braces
    | atom # AtomX
    ;

atom
    : iexp # IExp
    ;

iexp
    : INT                  # Int
    ;